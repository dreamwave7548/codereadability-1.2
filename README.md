# CodeReadability

## 簡介
這是判斷一個程式碼在使用者的命名上是否有遵循各個類型的規範以及使用單字的命名，來達成其他人看此程式碼的理解程度多寡。

## 事前安裝工具
```
pip install AST #抽象語法樹
pip install regex #正規表達式
pip install nltk #自然語言處理
nltk.download()
```

## 使用方式
```
python codereadability.py example1.py
```
## 變數命名規範
* ClassDef : 第一個英文字母必須大寫，其餘小寫，後面還有大寫的話，後面必須要有小寫英文字母，而中間不可加底線 ex. Animal  ClassName
* Functiondef : 全部小寫，中間可加底線；雙底線後有文字之後再雙底線;新增第一個字為底線之後全英文 ex. who  __ init __ _fibo
* Assign : 全部小寫、中間可加底線 ex. name var_name
* Attribute: 同Assign ex. name var_name 
 
## 成績評斷說明
* regexScore = Re有中的總數 / 所有命名的總數
* dictScore = 有中單字的總數 / 所有都拆開過的命名總數
* 總成績 = 0.7 * regexScore + 0.3 * dictScore

## 結果輸出
```
------ 照特定格式佔70% 單字上的命名佔30%  ------
All Name: {...}
Good Format: {...}
Not Good Format: {...}
Good Word: {...}
Not Good Word: {...}
分數: XX.X
---------------------- End ----------------------
```
