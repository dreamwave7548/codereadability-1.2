
import csv

import pandas as pd
import numpy  as np
import re

# https://medium.com/@chiayinchen/%E4%BD%BF%E7%94%A8-python-pandas-%E5%BE%9E%E5%8C%85%E5%90%AB%E5%A4%9A%E5%80%8B%E5%B7%A5%E4%BD%9C%E8%A1%A8%E7%9A%84-excel-%E4%B8%AD%E6%93%B7%E5%8F%96%E6%95%B8%E6%93%9A-f9fcf507e05b
# https://medium.com/datainpoint/%E5%BE%9E-pandas-%E9%96%8B%E5%A7%8B-python-%E8%88%87%E8%B3%87%E6%96%99%E7%A7%91%E5%AD%B8%E4%B9%8B%E6%97%85-8dee36796d4a

# https://medium.com/views-from-bi-pm/%E5%88%86%E4%BA%AB-%E7%94%A8-python-%E7%B7%A8%E8%BC%AF-excel-%E5%AF%AB%E5%85%A5%E8%B3%87%E6%96%99%E5%92%8C%E7%95%AB%E5%9C%96%E8%A1%A8%E9%83%BD%E8%B6%85%E5%A5%BD%E4%B8%8A%E6%89%8B%E7%9A%84-xlsxwriter-%E5%A5%97%E4%BB%B6-d47cf3aa9c3d

# ROOTPATH = 'python_exam.csv'
ROOTPATH = 'python_exam.xlsx'
# data = pd.read_excel(ROOTPATH, usecols = [11] , skiprows = 2, sheet_name = None)

data = pd.read_excel(ROOTPATH)
codes = data['code']
# stdscore = data[['id', 'code']]

# i=0
for co in codes:
    print(co)
    print('=============================')

    with open('./samples/python'+str(i)+'.py', 'w', newline='') as file:
        # file.writelines( str(co.encode(encoding="utf-8")) )
        # file.write(str( co.encode(encoding="cp950") ))
        file.write(str( co.encode("utf8").decode("cp950", "ignore") ))

    i=i+1

# df = pd.read_excel(ROOTPATH, usecols = [11], skiprows = 2)
# print(df.shape)
# i=0
# for d in df.iterrows():
#     print("d"+str(i)+": " , d)
#     with open('./samples/'+str(i)+'.py', 'w', newline='') as file:
#         # writer = csv.writer(file)
#         # # 寫入二維表格
#         # writer.writerows(d)
#         # file.writelines(str(d))
#         file.write(str(d))
#     i+=1
