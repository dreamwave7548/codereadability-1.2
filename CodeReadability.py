
import re
import ast
import argparse 
from nltk.corpus import wordnet
import nltk

# '''讀參數'''
# arg_parse = argparse.ArgumentParser(description='A static Big-O analysis tool base on Big-O AST.')
# arg_parse.format_help()
# arg_parse.add_argument('filename', type=str, help='target code filename')
# args = arg_parse.parse_args()
#
# '''讀檔案'''
# source_file_name = args.filename
#
# f = open(source_file_name, "r")
#
# r_node = ast.parse(f.read())


'''走訪AST'''
class CodeReadability(ast.NodeVisitor):
    '''第一步'''
    def visit_Module(self, node):
        self.Allname = set()    # 所有命名名稱
        self.Splitname = set()  # 所有切開命名名稱
        self.HitRegexName = set()   # 符合regex格式命名
        self.NHitRegexName = set()  # 不符合regex格式命名
        self.Wordname = set()   # 符合單字命名
        self.NWordname = set()  # 不符合單字命名
        
        self.generic_visit(node)
        '''單字對照'''
        for i in self.Splitname :
            if wordnet.synsets(i.lower()):
                self.Wordname.add(i)
            else:
                self.NWordname.add(i)

        '''輸出'''
        print('------ 照特定格式佔70% 單字上的命名佔30%  ------')
        print("All Name:",self.Allname)
        print("Good Format:",self.HitRegexName)
        print("Not Good Format:",self.NHitRegexName)
        print("Good Word:",self.HitRegexName)
        print("Not Good Word:",self.NWordname)

        '''評分標準'''
        if((float)(len(self.Allname))==0):
            regexScore = 0
        else:
            regexScore = (float)(len(self.HitRegexName)) / (float)(len(self.Allname))
        if((float)(len(self.Splitname)) == 0):
            dictScore = 0
        else:
            dictScore = (float)(len(self.Wordname)) / (float)(len(self.Splitname))
        Score = regexScore * 0.7 + dictScore * 0.3
        print("分數:",round(Score*100, 2))
        print('---------------------- End ----------------------')

        comment_text = list()
        comment_text.append('------ 照特定格式佔70% 單字上的命名佔30%  ------')
        comment_text.append("All Name:" + str(self.Allname))
        comment_text.append("Good Format:" + str(self.HitRegexName))
        comment_text.append("Not Good Format:" + str(self.NHitRegexName))
        comment_text.append("Good Word:" + str(self.HitRegexName))
        comment_text.append("Not Good Word:" + str(self.NWordname))
        comment_text.append("分數:" + str(round(Score*100, 2)) )
        # print(comment_text )

        return comment_text

    '''Function判斷'''
    def visit_FunctionDef(self, node):
        self.generic_visit(node)
        self.Allname.add(node.name)
        if(re.match('([a-z]+_{0,1}[a-z]+)|(__.*__)|(_[a-z]+)$',node.name)):
            self.HitRegexName.add(node.name)
            split_words = re.findall('[a-z]+',node.name)
            for i in split_words :
                self.Splitname.add(i)
            split_words.clear()
        else:
            self.NHitRegexName.add(node.name)
        
    '''Class判斷'''
    def visit_ClassDef(self, node):
        self.generic_visit(node)
        self.Allname.add(node.name)
        if(re.match('([A-Z]{1}[a-z]+[A-Z]{1}[a-z]+)|([A-Z]{1}[a-z]+)$',node.name)):
            self.HitRegexName.add(node.name)
            split_words = re.findall('[A-Z]{1}[a-z]+',node.name)
            for i in split_words :
                self.Splitname.add(i)
            split_words.clear()
        else:
            self.NHitRegexName.add(node.name)

    '''Assign判斷'''
    def visit_Assign(self, node):
        self.generic_visit(node)
        for target in node.targets:
            if isinstance(target, ast.Name):
                self.Allname.add(target.id) 
                if(re.match('[a-z]+_{0,1}[a-z]+$',target.id)):
                    self.HitRegexName.add(target.id)
                    split_words = re.findall('[a-z]+',target.id)
                    for i in split_words :
                        self.Splitname.add(i)
                    split_words.clear()
                else:
                    self.NHitRegexName.add(target.id)
                    
    '''Attr判斷'''
    def visit_Attribute(self,node):
        self.Allname.add(node.attr)
        if(re.match('[a-z]+_{0,1}[a-z]+$',node.attr)):
            self.HitRegexName.add(node.attr) 
            split_words = re.findall('[a-z]+_{0,1}[a-z]+$',node.attr)
            for i in split_words :
                self.Splitname.add(i)
            split_words.clear()
        else:
            self.NHitRegexName.add(node.attr)
            
def code_readability_calc(ROOTPATH):
    source_file_name = ROOTPATH
    f = open(source_file_name, "r")
    r_node = ast.parse(f.read())
    #     '''輸出結果'''
    CodeReadability().visit(r_node)



if __name__ == '__main__':

    # '''讀參數'''
    # arg_parse = argparse.ArgumentParser(description='A static Big-O analysis tool base on Big-O AST.')
    # arg_parse.format_help()
    # arg_parse.add_argument('filename', type=str, help='target code filename')
    # args = arg_parse.parse_args()
    # source_file_name = ROOTPATH
    #
    # f = open(source_file_name, "r")
    #
    # r_node = ast.parse(f.read())
    # #     '''輸出結果'''
    # CodeReadability().visit(r_node)

    '''讀檔案'''
    ROOTPATH = './examples/colab-py/example1.py'
    comment_text = code_readability_calc(ROOTPATH)
