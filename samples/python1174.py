def fibo(n):    
    a = 0
    b = 1
    if(n == 0):
        return 0
    elif(n == 1):
        return 1
    else:
        for i in range(n-1):
            ans = a+b
            a = b
            b = ans
        
        return ans
    
print(fibo(10))
