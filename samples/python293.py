n = int (10)
pn=[0] * n
i = 0

while n != 0:
    if n % 2 == 1:
        pn[i] = 1
    n = n // 2
    i = i + 1

pn.reverse()
print("".join('%s' %n for n in pn))