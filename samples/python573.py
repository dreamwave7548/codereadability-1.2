def factorial(n): 
    temp = 1
    res = 1
    for i in range(2, n+1): 
        res = res * i 
        temp = temp +  res
        
    return temp
  
# Function to calculate required 
# series 

  
# Drivers code 

print("1! + 2! + 3! + ......+ {0}! = {1}" .format(10, factorial(10)))