def fibo(n, fib = [0, 1]):    
    if n not in fib:
        for i in range(len(fib), n + 1):
            fib.append(fib[i - 1] + fib[i - 2])
    return fib[n]
print(fibo(10))
