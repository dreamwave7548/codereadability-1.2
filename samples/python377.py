n = 10
ans = 0
for i in range(1, n+1):
    temp = 1
    for j in range(1, i+1):
        temp *= j
    ans += temp
        
print("1! + 2! + 3! + ......+ {0}! = {1}" .format(10, ans))