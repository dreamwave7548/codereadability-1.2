class Account:
    def __init__(self, name):
        self.name = name
        self.__balance = 0

    def deposit(self, amount):
        # print('{}��NT${:,.0f}'.format(self.name, amount))
        self.__balance += amount
        self.show()

    def withdraw(self, amount):
        if self.__balance - amount >= 0:
            self.__balance -= amount
            self.show()
        else:
            print('{}�款��.'.format(self.name))

    def show(self):
        print("{}��NT${:,.0f}".format(self.name, self.__balance))


userA = Account("Jack")
userA.withdraw(1000)
userA.deposit(5000)
userA.withdraw(1000)
userA.show()