n = int(10)
pn=[0] * n
i = len(pn)-1

while (n//2) != 0:
    remain = n%2
    n = (n//2)
    pn[i] = remain
    i -= 1

if ((n//2) == 0) and (n != 0):
    remain = n%2
    n = (n//2)
    pn[i] = remain
    i -= 1

print(pn)