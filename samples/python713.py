import sys

class Account:
    def __init__(self, name):
        self.name = name
        self.__balance = 0
    
    def deposit(self, amount):
        if amount <= 0:
            raise ValueError('must be positive')
        else:
            self.__balance += amount
            print('{}��NT${}'.format(self.name, amount))
    
    def withdraw(self, amount):
        if amount <= self.__balance:
            self.__balance -= amount
            print('{}�NT${}'.format(self.name, amount))
        else:
            print('{}�款��'.format(self.name))
    
    def show(self):
        print('{}��NT${}'.format(self.name, self.__balance))

if __name__ == '__main__':
    userA = Account('Jack')
    userA.withdraw(1000)
    userA.deposit(5000)
    userA.withdraw(1000)
    userA.show()