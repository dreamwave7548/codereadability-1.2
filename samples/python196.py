def fibo(x,a=0,b=1):
    if x==0:
        return a
    if x==1:
        return b

    return fibo(x-1,b,a+b)

print(fibo(10))
