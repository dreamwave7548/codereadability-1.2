import sys

def fibo(n:int, a = 0, b = 1):
    if n == 0:
        return a
    if n == 1:
        return b
    else:
        return fibo(n - 1, b, a + b)

if __name__ == '__main__':
    print(fibo(10))