class Account:
    def __init__(self, name):
        self.name = name
        self._balance = 0
        
    def deposit(self, amount):
        print('{}��NT${:.0f}.'.format(self.name, amount))
        self._balance += amount
      
    def withdraw(self, amount):
        if amount <= self._balance:
            print('{}�NT${:.0f}.'.format(self.name, amount))
            self._balance -= amount
        else:
            print('{}�款��.'.format(self.name))
    
    def show(self):
        print("{}��NT${:.0f}.".format(self.name, self._balance))
        
        
userA = Account("Jack")
userA.withdraw(1000)
userA.deposit(5000)
userA.withdraw(1000)
userA.show()