def fibo(n):    
    if n<0: 
        print("Incorrect input") 
    # First fibo number is 0 
    elif n==1: 
        return 0
    # Second fibo number is 1 
    elif n==2: 
        return 1
    else: 
        return fibo(n-1)+fibo(n-2) 

print(fibo(10))
