def fibo(n):    
    f_0 = 0
    f_1 = 1
    f_now = 0
    if n == 0:
        return f_0
    elif n == 1:
        return f_1
    elif n >= 2:
        for i in range(1, n):
            f_now = f_0 + f_1
            f_0 = f_1
            f_1 = f_now

    return f_now

print(fibo(10))
