n = int(10)
pn = [0] * n
i = 0
ans = ""
# while start:
while(n):
    ans = str(n & 1) + ans
    n //= 2
print(ans)
# while end
