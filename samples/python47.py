def fibo(n):
    a=0
    b=1
    if n==0 or n==1:
        return n
    for i in range(2,n+1):
        c=b
        b=a+b
        a=c
    return b

print(fibo(10))
