def fibo(n):    
    return 1 if n==0 or n==1 else fibo(n-1) + fibo(n-2)

print(fibo(10))
