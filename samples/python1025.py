n = int (10)
pn=[0] * n
i = 0
while n>0:
    pn[i] = n%2
    i += 1
    n = int(n/2)
pn.reverse()
i = 0
while pn[i]==0:
    del pn[i]
strings = [str(x) for x in pn]
print("".join(strings))