
result = ""
num = 10
def dec_to_bin(num):
    bin = []
    while num / 2 > 0:
        bin.append(str(num % 2))
        num = num // 2
    bin.reverse()
    return ''.join(bin)
print (dec_to_bin(10))
