n = int (10)
pn=[0] * n
i = 0
r = 0
while n > 0:
    pn[i] = n % 2
    r += pn[i]
    n = n // 2
    r *= 10
    i += 1
print(r)