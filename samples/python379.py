def fibo(n):    
    return 1 if n==1 or n==2 else fibo(n-1) + fibo(n-2)

print(fibo(10))
