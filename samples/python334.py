# 設��n10
n = int(10)


def factorial(n, total=1):
    if n == 0:
        return total
    else:
        return factorial(n-1, total*n)


def superCOOL(n):
    if (n == 0):
        return 1
    ans = 0
    for i in range(n):
        # print(f"{i+1}! {factorial(i+1)}")
        ans += factorial(i+1)
    return ans
# for start:


# for end
print("1! + 2! + 3! + ......+ {0}! = {1}".format(n, superCOOL(n)))
