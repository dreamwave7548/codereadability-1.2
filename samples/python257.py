n = int (10)
pn=[0] * n
i = 0
#while start:
d = n
while d>0:
    r = d%2
    d = d//2
    pn[i] = r
    i += 1
#while end
n -= 1
while n > 0 and pn[n] == 0:
    n -= 1

result = ''
for i in range(n+1):
    result =  str(pn[i]) +  result
print(result)